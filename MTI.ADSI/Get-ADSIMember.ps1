﻿Function Get-ADSIMember($Grouppath){
    $groupObj = [ADSI]$Grouppath

    foreach($member in $groupObj.Member){ 
        $userPath = "LDAP://" + $member
        $UserObj = [ADSI]$userPath
        If($UserObj.groupType.Value -eq $null){
            if($repeathashUser.Contains($UserObj.distinguishedName.ToString()) -eq $false){
                $repeathashUser.add($UserObj.distinguishedName.ToString()) | Out-Null
            }
        }
        Else{
            if($repeathashGroup.Contains($UserObj.distinguishedName.ToString()) -eq $false){
                $repeathashGroup.add($UserObj.distinguishedName.ToString()) | Out-Null
                Get-ADSIMember("LDAP://$($UserObj.distinguishedName)") 
            }
        }
    }
}