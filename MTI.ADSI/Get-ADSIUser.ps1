﻿Function Get-ADSIUser ($SAMName){
 
    $root = [ADSI]''
    $searcher = new-object     System.DirectoryServices.DirectorySearcher($root)
    $searcher.filter = "(&(objectClass=User)(sAMAccountName=$SAMName))"
    $user = $searcher.findall()

    if ($user.count -eq 1){     
        return $user[0]
    }
}