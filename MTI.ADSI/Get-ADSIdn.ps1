﻿Function Get-ADSIdn ($SAMName,
                $objectType){
 
    $root = [ADSI]''
    $searcher = new-object     System.DirectoryServices.DirectorySearcher($root)
    $searcher.filter = "(&(objectClass=$objectType)(sAMAccountName=$SAMName))"
    $user = $searcher.findall()

    if ($user.count -eq 1){     
        return $user[0].path
    }
}