﻿Function Get-ADSIsam ($dnName){
 
    $root = [ADSI]''
    $searcher = new-object     System.DirectoryServices.DirectorySearcher($root)
    $searcher.filter = "(&(distinguishedName=$dnName))"
    $user = $searcher.findall()

    if ($user.count -eq 1){     
        return $user[0].Properties.samaccountname
    }
}