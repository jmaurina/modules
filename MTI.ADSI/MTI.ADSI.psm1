﻿param(
	$RunMode
)

If( $RunMode -NotIn ('Production','Debug') ){
	#Keep this GUID so it's easier to find all PSM1 copies in Source Control: 1022f371-0435-4a3c-b117-22f8452bdf02
	
	#Do not use 'PrimalScriptHostImplementation'.  There is not a diferentiator for the EXE and Sapien IDE.  This causes slow module loading on any PS Interface
	If( $host.Name -in ('Windows PowerShell ISE Host','Visual Studio Code Host','PowerShell Tools for Visual Studio Host') ){
		$RunMode = 'Debug'
	}
	Else{
		$RunMode = 'Production'
	}
}


If( $RunMode -eq 'Debug' ){

	Write-Warning 'Loading module in debug mode.  This may increase module loading times.'

	#Slow way of loading a module.  It also has concurrency issues.
	Get-ChildItem -Path $PSScriptRoot\*.ps1 -Exclude '*.Tests.ps1' | ForEach-Object { . $_.FullName }

}
Else{

	#Very fast way of loading a module.  Pain in the ass to debug through.
	$mvModuleFiles = Get-ChildItem -Path $PSScriptRoot\*.ps1 -Exclude '*.Tests.ps1'

	Foreach($mvModuleFile in $mvModuleFiles){
		
		$mvModuleFileStream = $null
		$mvModuleFileStreamReader = $null
		$mvModuleFileScript = $null
		$mvModuleFileScriptBlock = $null
		
		If([System.IO.File]::Exists($mvModuleFile.FullName)){
			
			#Read the file with no file locking.
			$mvModuleFileStream = [System.IO.File]::Open($mvModuleFile.FullName, 'Open', 'Read', 'ReadWrite')
			$mvModuleFileStreamReader = New-Object System.IO.StreamReader($mvModuleFileStream)
			$mvModuleFileScript = $mvModuleFileStreamReader.ReadToEnd()
			$mvModuleFileStream.Close()
			$mvModuleFileStreamReader.Close()
			
			#Convert the Text to a ScriptBlock
			$mvModuleFileScriptBlock = [scriptblock]::Create($mvModuleFileScript)
			#Import Script Block into Current Scope
			Invoke-Command -ScriptBlock $mvModuleFileScriptBlock -NoNewScope -ArgumentList $PSScriptRoot
		}
	}


	#Remove helper variables
	Remove-Variable -Name foreach, mvModuleFile, mvModuleFiles, mvModuleFileStream, mvModuleFileStreamReader, mvModuleFileScript, mvModuleFileScriptBlock
}

Export-ModuleMember -Function * -Alias * -Variable *