﻿Function Test-ADSIComputerIsGroupMember
{
    param($_GroupName)

	$ADGroupDN = Get-ADSIdn $_GroupName "group"
	$GroupMembers = Get-ADSIMemberCaller $ADGroupDN | foreach{ ($_.split(',')[0]).Replace('CN=', '') }
	
    $ComputerName = $env:COMPUTERNAME

	If ($GroupMembers -contains $ComputerName) { $IsMemberOfGroup = $true }
	Else { $IsMemberOfGroup = $false }

    RETURN $IsMemberOfGroup

}
