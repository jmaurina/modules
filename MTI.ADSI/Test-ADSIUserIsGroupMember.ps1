Function Test-ADSIUserIsGroupMember
{
    param($_GroupName)

	$ADGroupDN = Get-ADSIdn $_GroupName "group"
	$GroupMembers = Get-ADSIMemberCaller $ADGroupDN | foreach{ ($_.split(',')[0]).Replace('CN=', '') }
	
    $UserDisplayName = $(Get-ADSIUser($env:UserName)).Properties['DisplayName']

	If ($GroupMembers -contains $UserDisplayName) { $IsMemberOfGroup = $true }
	Else { $IsMemberOfGroup = $false }

    RETURN $IsMemberOfGroup

}
